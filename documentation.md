Documentation du controlleur :
------------------------------

## Constructeur
Paramètres :
  * Game game : modèle de la partie de jeu
  * int pointsToWin : nombre de points nécessaires pour gagner
  * int coinsToAlign nombre de jetons à aligner pour marquer un point
Description : Crée l'instance du controleur de jeu.
  
## ``playCoin(int coinIndex, int column)``
Paramètres :
  * int coinIndex : Indice dans le stack du joueur courant du jeton à jouer
  * int column : Colonne sur laquelle il souhaite jouer

Description : Fais les vérifications nécessaires puis fais jouer le joueur courant, le jeton renseigné sur la colonne renseignée.


## ``popCoin(int column)``
Paramètres :
  * int column : Colonne sur laquelle il souhaite enlever un jeton de sa couleur

Description : Fais les vérifications nécessaires puis fais éjecte le jeton demandé si nécessaire.
 

## ``getCurrentPlayer()``
Retour : Player player : Joueur Courant
Description : Permet de récupérer le joueur qui doit jouer.



## ``getCoinsToAlign()``
Retour : int cta : Nombre de jeton aligné

Description : Permet de récupérer le nombre de jeton à aligner pour gagner.



## ``nextCoinPowerUp()``
Description : Passe au type de jeton suivant