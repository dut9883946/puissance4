package observersubject;

public interface Observer {
	
	public void update();

}
