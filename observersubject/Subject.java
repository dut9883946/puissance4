package observersubject;

import java.util.ArrayList;
import java.util.List;

public abstract class Subject {
	
	List<Observer> observers = new ArrayList<>( ); 
	
	protected void notifyObservers() {
		
		for( int i = 0; i < observers.size(); i++ )
			observers.get(i).update();
		
	}
	
	
	public void ajouterObservateur( Observer o ) {
		
		observers.add( o );
		
	}
	
	
	public void retirerObservateur( Observer o ) {
		
		observers.remove( o );
		
	}

}
