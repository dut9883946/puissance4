# Connect Four

Projet :
--------
`Le but du projet est de réaliser une application permettant à deux joueurs de jouer au jeu Puissance 4.`

Travail demandé :
-----------------
* Le code source du jeu.
* Un compte rendu des choix de conception.
* Une documentation qui permette facilement de développer d’autres interfaces graphiques.
* Un diagramme de classe de conception pour chaque variante non implémentée.
* Un executable au format jar.

