Compte rendu des choix de conception :
--------------------------------------

Dans un premier temps, nous avons imaginés la structure du projet et réalisé les diagrammes de classes de conception.

Par la suite, nous avons commencé par développer le modèle du jeu de puissance 4.
Nous avons fait le choix de créer des jetons qui puissent avoir un attribut powerUp, mais qui ne serait pas utilisé dans les modes de jeu utilisants les jetons 'classiques'.

Puis, nous avons développé le template de controleur qui servirait de base à l'implémentation de chaque mode de jeu.
Après avoir testé et débuggé le controleur et le modèle en lignes de commandes, nous avons créé le template de vue.

Pour ce qui est de la cohérence des données, nous avons mis en place un patron sujet / observeur.

Après avoir terminé et testé le mode de jeu 'classique', nous avons implémentés les autres modes de jeu, en créant si nécessaire un controleur et une vue dédié.

Par manque de temps, nous n'avons pas terminé le mode de jeu 'power up', et avons privilégiés les tests des autres modes de jeu.
