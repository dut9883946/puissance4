package connectfour.connectfourmodel;

/**
 * Modélise la "couleur" d'un jeton de puissance 4.
 */
public enum Color {
  
  YELLOW,

  RED;
};
