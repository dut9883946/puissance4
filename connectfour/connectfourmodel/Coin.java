package connectfour.connectfourmodel;

/**
 * Modélise un jeton dans le jeu de puissance 4.
 */
public class Coin {
  
  private Color color;
  private PowerUp powerUp;

  /**
   * Créé un jeton de la couleur donnée.s
   * 
   * @param color couleur du jeton
   */
  public Coin(Color color) {
    this(color, null);
  }

  public Coin(Color color, PowerUp powerUp) {
    this.color = color;
    this.powerUp = powerUp;
  }

  /**
   * Renseigne la couleur du jeton.
   */
  public Color getColor() { return color; }

  /**
   * Renseigne le power up du jeton.
   */
  public PowerUp getPowerUp() { return powerUp; }

}