package connectfour.connectfourmodel;

import java.util.List;

/**
 * Modélise un joueur du jeu de puissance 4.
 */
public class Player {

  private Color color;
  private List<Coin> coins;
  private int points;

  /**
   * Construit un joueur avec la couleur qui lui est assignée et une liste de jetons restons
   * 
   * @param color couleur à assigner au joueur
   * @param coins liste de jetons
   */
  public Player(Color color, List<Coin> coins) {
    this.color = color;
    this.coins = coins;
    this.points = 0;
  }

  /**
   * Renseigne la couleur de jeton associée au joueur
   * 
   * @return couleur demandée
   */
  public Color getColor() { return color; }

  /**
   * Renvoies le nombre de points du joueur
   * 
   * @return entier représentant les points
   */
  public int getPoints() { return points; }

  /**
   * Renseigne les jetons restants au joueur
   * 
   * @return tableau contenant les jetons
   */
  public int getCoinNumber() { return coins.size(); }

  /**
   * Renvoie vrai si le joueur a encore des jetons spéciaux
   * 
   * @return vrai s'il reste des jetons spéciaux
   */
  public boolean stillHaveSpecialCoins() {
    boolean res = false;
    for(Coin c : coins)
      if(c.getPowerUp() != null)
        res = true;
    return res;
  }

  /**
   * Renseigne le jeton d'indice demandé.
   * 
   * @param index indice du jeton demandé
   * @return jeton d'indice demandé
   */
  public Coin getCoin(int index) { return coins.get(index); }

  /**
   * Ajoute un point au score du joueur
   */
  public void score() { points++; }

  /**
   * Joue le jeton d'index demandé
   * 
   * @param coinIndex index du jeton à jouer
   */
  public void playCoin(int coinIndex) { coins.remove(coinIndex); }

  /**
   * Récupère le jeton donné en paramètres.
   * 
   * @param c jeton à récupérer
   */
  public void addCoin(Coin c) { coins.add(c); }
  
}
