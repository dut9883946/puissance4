package connectfour.connectfourmodel;

import java.util.Objects;

/**
 * Modélise une case du jeu de puissance 4.
 */
public class Square {

  /**
   * Jeton placé dans la case
   */
  private Coin coin;

  /**
   * Créé une case vide
   */
  public Square() { }

  /**
   * Créé une case avec un jeton placé dessus
   * 
   * @param coin jeton placé sur la case
   */
  public Square(Coin coin) { setCoin(coin); }

  /**
   * Renseigne le jeton placé sur la case
   * 
   * @return jeton placé sur la case
   */
  public Coin getCoin() { return coin; }

  /**
   * Place sur la case le jeton donné
   * 
   * @param coin jeton à placer sur la case
   */
  public void setCoin(Coin coin) { this.coin = coin; }
  
}
