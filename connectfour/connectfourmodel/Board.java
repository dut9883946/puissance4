package connectfour.connectfourmodel;

import java.util.Arrays;

/**
 * Modélise la grille du jeu de puissance 4.
 */
public class Board {

  /**
   * Nombre de colonnes du plateau
   */
  private final int columns;

  /**
   * Nombre de lignes du plateau
   */
  private final int rows;

  /**
   * Tableau représentant les cases du plateau
   */
  private Square[][] squares;

  /**
   * Créé une grille de jeu des dimensions données.
   * 
   * @param columns nombre de colonnes
   * @param rows nombre de lignes
   */
  public Board(int columns, int rows) {
    this.rows = rows;
    this.columns = columns;
    this.squares = new Square[rows][columns];
    for(Square[] row : squares)
      for(int i=0; i< columns; i++)
        row[i] = new Square();
  }

  /**
   * Renseigne le nombre de lignes de la grille.
   */
  public int getNbRows() { return rows; }
  
  /**
   * Renseigne le nombre de colonnes de la grille.
   */
  public int getNbColumns() { return columns; }

  /**
   * Renseigne le tableau de cases à deux dimensions.
   */
  public Square[][] getAllSquares() { return squares.clone(); }

  /**
   * Renseigne la case demandée.
   * 
   * @param row ligne de la case
   * @param column colonne de la case
   * @return instance de la case
   */
  public Square getSquare(int row, int column) { return squares[row][column]; }

  /**
   * Renvoie vrai si le plateau est rempli, faux sinon.
   * 
   * @return vrai si le plateau est rempli
   */
  public boolean isFull() { return getEmptySquares()==0; }

  /**
   * Renvoie le nombre de cases libres du plateau.
   * 
   * @return nombre de cases vides
   */
  public int getEmptySquares() {
    int res = 0;
    for(Square[] row : squares)
      for(Square square : row)
        if(square.getCoin() == null)
          res++;
    return res;
  }

  /**
   * Retourne la ligne en cours de remplissage.
   * 
   * @return première ligne comportant au moins une case vide.
   */
  public int getFirstPlayableRow(){
    int row = -1;
      for(int i=0; i<rows && row == -1; i++)
        for(int y=0; y<columns && row == -1;y++)
          if(getSquare(i, y).getCoin() == null)
            row = i;
    return row;
  }

  /**
   * Renvoie une description de l'état du plateau sous forme de chaîne de caractères.
   * 
   * @return chaîne de caractères décrivant le plateau de jeu
   */
  @Override
  public String toString() {
    String res = "BOARD :\n";
    for(int i=rows-1; i >= 0; i--) {
      res += "- ";
      for(int y=0; y< columns; y++) 
        res += getSquare(i, y).getCoin() == null ? "  " : getSquare(i, y).getCoin().getColor() == Color.RED ? "x " : "o ";
      res += "\n";
    }
    return res;
  }
  
}
