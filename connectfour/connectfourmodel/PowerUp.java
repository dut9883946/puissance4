package connectfour.connectfourmodel;

public enum PowerUp {

  PLAY2,

  ANVIL,

  WALL,

  BOMB;
  
};
