package connectfour.connectfourmodel;

import java.util.ArrayList;
import java.util.List;

/**
 * Modélise une partie de jeu de puissance 4.
 */
public class Game {

  private Player[] players;
  private Board board;
  private Player winner;
  private boolean isOver;

  /**
   * Construit une nouvelle partie avec un plateau et un tableau de joueurs en paramètres.
   * 
   * @param board plateau de jeu
   * @param players tableau de joueurs
   */
  public Game(Board board) {
    this.board = board;
    isOver = false;
    players = new Player[2];
    for(int i=0; i<2; i++)
      players[i] = new Player(Color.values()[i], createCoinStack(Color.values()[i]));
  }

  /**
   * Renseigne le joueur d'index demandé
   * 
   * @param index index du joueur demandé
   */
  public Player getPlayer(int index) { return players[index]; }

  /**
   * Renseigne le plateau de jeu utilisé
   * 
   * @return plateau de jeu
   */
  public Board getBoard() { return board; }

  /**
   * Renseigne le joueur ayant gagné la partie
   * 
   * @return gagnant de la partie
   */
  public Player getWinner() { return winner; }

  /**
   * Renseigne le caractère terminé ou non de la partie
   * 
   * @return vrai si la partie est terminée
   */
  public boolean isOver() { return isOver; }

  /**
   * Définit le joueur ayant gagné la partie
   * 
   * @param player
   */
  public void setWinner(Player player) { winner = player; }

  /**
   * Définit la partie comme terminée
   */
  public void setIsOver() { isOver = true; }
  
  /**
   * Crée une liste de jetons pour une couleur donnée
   * 
   * @param color couleur des jetons
   * @return liste finale
   */
  private List<Coin> createCoinStack(Color color) {
    List<Coin> stack = new ArrayList<Coin>();
    for(int i=0; i< 51; i++)
      stack.add(new Coin(color));
    for(PowerUp powerUp : PowerUp.values())
      stack.add(new Coin(color, powerUp));
    return stack;
  }
}
