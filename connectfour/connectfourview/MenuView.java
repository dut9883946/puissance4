package connectfour.connectfourview;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import connectfour.connectfourclassic.ClassicGameView;
import connectfour.connectfourclassic.ConnectFourController;
import connectfour.connectfourcontroller.TemplateController;
import connectfour.connectfourmodel.Board;
import connectfour.connectfourmodel.Game;
import connectfour.connectfourpop10.Pop10Controller;
import connectfour.connectfourpop10.Pop10View;
import connectfour.connectfourpopout.PopOutController;
import connectfour.connectfourpopout.PopOutView;
import connectfour.connectfourpowerup.PowerUpController;
import connectfour.connectfourpowerup.PowerUpView;

public class MenuView extends JFrame {
  
  public MenuView() {
    setLayout(new GridLayout(3, 1));

    //Titre
	  ImageIcon jetonRouge = new ImageIcon(getClass().getClassLoader().getResource("image/jetonRouge.png"));
	  JLabel title = new JLabel("  Puissance 4",jetonRouge,JLabel.CENTER);
    title.setFont(new Font("Arial", Font.BOLD, 60));
    add(title);

    JPanel settings = new JPanel();

    //Mode de jeu
    String[] dimensions = { "7 x 6", "6 x 5", "8 x 7", "9 x 7", "10 x 7", "8 x 8" };
    JComboBox dimension = new JComboBox(dimensions);

    JPanel modeWrapper = new JPanel();
    JLabel modeTitle = new JLabel("Mode de jeu : ");
    modeTitle.setFont(new Font("Arial", Font.BOLD, 20));
    String[] modes = { "Classic", "Pop-out", "Power-up", "Pop-10", "5-in-a-row" };
    JComboBox mode = new JComboBox(modes);
    mode.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent arg0) {  
        if(mode.getSelectedItem()=="5-in-a-row"){
		  dimension.removeAllItems();
		  dimension.addItem("9 x 6");
		  dimension.setSelectedItem("9 x 6");
		}
		else
			if(dimension.getItemCount()==1){
				dimension.removeAllItems();
				for(String val : dimensions){
					dimension.addItem(val);
				}
			}
      }    
    });
    modeWrapper.add(modeTitle);
    modeWrapper.add(mode);
    settings.add(modeWrapper);

    //Dimensions du plateau
    JPanel dimensionWrapper = new JPanel();
    JLabel dimensionTitle = new JLabel("  Dimensions : ");
    dimensionTitle.setFont(new Font("Arial", Font.BOLD, 20));
    dimensionWrapper.add(dimensionTitle);
    dimensionWrapper.add(dimension);
    settings.add(dimensionWrapper);

    add(settings);

    //Bouton
    JPanel btnWrapper = new JPanel();
    JButton play = new JButton("Jouer");
    play.setFont(new Font("Arial", Font.BOLD, 30));
    btnWrapper.add(play);
    add(btnWrapper);
    play.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        Game g;
        TemplateController c;
        TemplateGameView v;

        //Taille de plateau :
        String dim = dimension.getSelectedItem().toString();
        int columns = Integer.parseInt(dim.split("x")[0].trim());
        int rows = Integer.parseInt(dim.split("x")[1].trim());
        g = new Game(new Board(columns, rows));

        //Mode de jeu :
        if(mode.getSelectedItem() == "Classic"){
          c = new ConnectFourController(g, 1, 4);
          v = new ClassicGameView(c, g);
        }
        else if(mode.getSelectedItem() == "Pop-out"){
          c = new PopOutController(g, 1, 4);
          v = new PopOutView((PopOutController)c, g);
        }
        else if(mode.getSelectedItem() == "Power-up"){
          c = new PowerUpController(g, 1, 4);
          v = new PowerUpView((PopOutController)c, g);
        }
        else if(mode.getSelectedItem() == "Pop-10"){
          c = new Pop10Controller(g, 10, 4);
          v = new Pop10View((Pop10Controller)c, g);
        }
        else if(mode.getSelectedItem() == "5-in-a-row"){
          c = new ConnectFourController(g, 1, 5);
          v = new ClassicGameView(c, g);
        }

        dispose();
      }
    });

    setTitle("Puissance 4");
    setSize(1000, 600);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setVisible(true);
  }
}
