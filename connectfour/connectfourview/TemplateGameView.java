package connectfour.connectfourview;


import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import connectfour.connectfourcontroller.TemplateController;
import connectfour.connectfourmodel.Game;
import connectfour.connectfourmodel.Color;
import observersubject.Observer;

public abstract class TemplateGameView extends JFrame implements Observer{

	protected TemplateController controleur;
	
	protected Game game;
	
	protected int square;

	protected JPanel barreJeton;

	protected JPanel bouton;

	protected JButton ajout;
	
	public TemplateGameView(TemplateController controleur,Game game) {
		this.controleur = controleur;
		this.game = game;
		this.square=0;
		
		controleur.ajouterObservateur(this);   
	    
		JPanel board=new JPanel();
		board.setLayout(new GridLayout(game.getBoard().getNbRows(), game.getBoard().getNbColumns()));
		ImageIcon boardImg = new ImageIcon(getClass().getClassLoader().getResource("image/CaseBlanche.png"));
		boardImg = new ImageIcon(boardImg.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT));
		for(int i=0;i<game.getBoard().getNbRows();i++) {
			for(int a=0;a<game.getBoard().getNbColumns();a++) {
				board.add(new JLabel(boardImg));
			}  	
		}
	    
		ImageIcon jetonImg = new ImageIcon(getClass().getClassLoader().getResource("image/jetonJaune.png"));
		jetonImg = new ImageIcon(jetonImg.getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT));
		JLabel jeton=new JLabel(jetonImg);
	  barreJeton=new JPanel(new BorderLayout());
		barreJeton.add(jeton, BorderLayout.WEST);
	  
		JButton gauche = new JButton("<"); 
		ajout = new JButton("Jouer"); 
		JButton droite = new JButton(">");
		bouton=new JPanel();
		gauche.setFont(new Font("Arial", Font.BOLD, 20));
		ajout.setFont(new Font("Arial", Font.BOLD, 20));
		droite.setFont(new Font("Arial", Font.BOLD, 20));
		gauche.addActionListener(onLeft);
		ajout.addActionListener(onPlay);
		droite.addActionListener(onRight);
		bouton.add(gauche);
		bouton.add(ajout);
		bouton.add(droite);
		
		JPanel jeu = new JPanel(); 
		jeu.setLayout(new BoxLayout(jeu, BoxLayout.Y_AXIS));
		jeu.setSize(80*game.getBoard().getNbColumns(),80*game.getBoard().getNbRows()+150);
		jeu.add(barreJeton);
		jeu.add(board);
		jeu.add(bouton);
	    
		JPanel jpPrincipal = (JPanel) this.getContentPane();
		jpPrincipal.setLayout(null);
		jpPrincipal.add(jeu);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(80*game.getBoard().getNbColumns()+150, 80*game.getBoard().getNbRows()+300);
		setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	@Override
	public void update() {
		JLabel jeton=(JLabel) ((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).getComponent(0);
		if(controleur.getCurrentPlayer().getColor()==Color.YELLOW){
				jeton.setIcon(new ImageIcon(new ImageIcon("image/jetonJaune.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
    	}
		else {
				jeton.setIcon(new ImageIcon(new ImageIcon("image/jetonRouge.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
		}
		
		JPanel board=(JPanel) ((JPanel)this.getContentPane().getComponent(0)).getComponent(1);
		int b=game.getBoard().getNbRows()-1;
	    for(int i=0;i<game.getBoard().getNbRows();i++,b--) {
	    	for(int a=0;a<game.getBoard().getNbColumns();a++) {
	    		if(game.getBoard().getSquare(b, a).getCoin()!=null) {
	    			if(game.getBoard().getSquare(b, a).getCoin().getColor()==Color.RED)
	    				((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseRouge.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));	    			
	    			else
							((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseJaune.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));	    			
					}
					else
					((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseBlanche.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
		    }  	
		}
		
		if(game.isOver()){
			((JPanel) this.getContentPane().getComponent(0)).setSize(board.getSize().width,board.getSize().height+((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).getSize().height);
			((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).removeAll();
			if(game.getWinner()!=null)
				((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).add(new JLabel("Le gagnant est le joueur "+(game.getWinner().getColor() == Color.YELLOW ? "jaune" : "rouge")));
			else
				((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).add(new JLabel("Égalité, personne n'a gagné"));
				
			((JLabel)((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).getComponent(0)).setFont(new Font("Arial", Font.BOLD, 24));
			((JLabel)((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).getComponent(0)).setHorizontalAlignment(JLabel.CENTER);
			((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(2)).setVisible(false);
		}

		//Vérifications du bouton jouer :
		int n = 0;
			while(n < game.getBoard().getNbRows() && game.getBoard().getSquare(n, square).getCoin() != null)n++;
			if(n == game.getBoard().getNbRows())
				ajout.setEnabled(false);
			else
				ajout.setEnabled(true);
	}

	/*	Event handlers 	*/

	protected ActionListener onLeft = new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
			if(square>0) {
				JLabel jeton=(JLabel) barreJeton.getComponent(0);
				jeton.setLocation(jeton.getLocation().x-80,10);	
				square--;
			}
			//Vérifications du bouton jouer :
			int n = 0;
			while(n < game.getBoard().getNbRows() && game.getBoard().getSquare(n, square).getCoin() != null)n++;
			if(n == game.getBoard().getNbRows())
				ajout.setEnabled(false);
			else
				ajout.setEnabled(true);

			update();
		}              
	};

	protected ActionListener onPlay = new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {  
			controleur.playCoin(0, square);
		}    
	};

	protected ActionListener onRight = new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
			if(square<game.getBoard().getNbColumns()-1) {
				JLabel jeton=(JLabel) barreJeton.getComponent(0);
				jeton.setLocation(jeton.getLocation().x+80,10);
				square++;
			}
			//Vérifications du bouton jouer :
			int n = 0;
			while(n < game.getBoard().getNbRows() && game.getBoard().getSquare(n, square).getCoin() != null)n++;
			if(n == game.getBoard().getNbRows())
				ajout.setEnabled(false);
			else
				ajout.setEnabled(true);
			
			update();
		}              
	};
	
}
