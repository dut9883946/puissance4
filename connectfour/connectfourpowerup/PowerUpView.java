package connectfour.connectfourpowerup;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import connectfour.connectfourmodel.Coin;
import connectfour.connectfourmodel.Color;
import connectfour.connectfourmodel.Game;
import connectfour.connectfourmodel.PowerUp;
import connectfour.connectfourpopout.PopOutController;
import connectfour.connectfourpopout.PopOutView;

public class PowerUpView extends PopOutView {

  protected JButton changerJeton;

  /**
   * @see PopOutView
   */
  public PowerUpView(PopOutController controleur, Game game) {
    super(controleur, game);
    changerJeton = new JButton("Jeton");
    changerJeton.setFont(new Font("Arial", Font.BOLD, 20));
    changerJeton.addActionListener(onChangeCoin);
    changerJeton.setEnabled(true);
    bouton.add(changerJeton, 3);
    this.setTitle("Power Up, "+game.getBoard().getNbColumns()+" x "+game.getBoard().getNbRows());
    this.setVisible(true);
  }

  @Override
  public void update() {
    JLabel jeton=(JLabel) ((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).getComponent(0);
    PowerUp p = ((PowerUpController)controleur).getCoin().getPowerUp();
    Color c = ((PowerUpController)controleur).getCoin().getColor();
		if(c == Color.YELLOW){
      if(p == null)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/jetonJaune.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
      else if(p == PowerUp.ANVIL)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/EnclumeJaune.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
      else if(p == PowerUp.BOMB)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/BombeJaune.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
      else if(p == PowerUp.PLAY2)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/X2Jaune.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
      else if(p == PowerUp.WALL)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/MurJaune.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
    }
		else {
      if(p == null)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/jetonRouge.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
      else if(p == PowerUp.ANVIL)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/EnclumeRouge.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
      else if(p == PowerUp.BOMB)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/BombeRouge.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
      else if(p == PowerUp.PLAY2)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/X2Rouge.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
      else if(p == PowerUp.WALL)
        jeton.setIcon(new ImageIcon(new ImageIcon("image/MurRouge.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT)));
    }
		
		JPanel board=(JPanel) ((JPanel)this.getContentPane().getComponent(0)).getComponent(1);
		int b=game.getBoard().getNbRows()-1;
	    for(int i=0;i<game.getBoard().getNbRows();i++,b--) {
	    	for(int a=0;a<game.getBoard().getNbColumns();a++) {
	    		if(game.getBoard().getSquare(b, a).getCoin()!=null) {
            c= game.getBoard().getSquare(b, a).getCoin().getColor();
            p= game.getBoard().getSquare(b, a).getCoin().getPowerUp();
	    			if(c==Color.RED){
              if(p == null)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseRouge.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
              else if(p == PowerUp.ANVIL)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseEnclumeRouge.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
              else if(p == PowerUp.BOMB)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseBombeRouge.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
              else if(p == PowerUp.PLAY2)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseX2Rouge.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
              else if(p == PowerUp.WALL)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseMurRouge.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
            }			
	    			else{
              if(p == null)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseJaune.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
              else if(p == PowerUp.ANVIL)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseEnclumeJaune.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
              else if(p == PowerUp.BOMB)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseBombeJaune.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
              else if(p == PowerUp.PLAY2)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseX2Jaune.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
              else if(p == PowerUp.WALL)
                ((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseMurJaune.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
            }  			
					}
					else
					((JLabel) board.getComponent(game.getBoard().getNbColumns()*(i)+a)).setIcon(new ImageIcon(new ImageIcon("image/CaseBlanche.png").getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT)));
		    }  	
		}
		
		if(game.isOver()){
			((JPanel) this.getContentPane().getComponent(0)).setSize(board.getSize().width,board.getSize().height+((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).getSize().height);
			((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).removeAll();
			if(game.getWinner()!=null)
				((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).add(new JLabel("Le gagnant est le joueur "+(game.getWinner().getColor() == Color.YELLOW ? "jaune" : "rouge")));
			else
			((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).add(new JLabel("Égalité, personne n'a gagné"));
			((JLabel)((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).getComponent(0)).setFont(new Font("Arial", Font.BOLD, 24));
			((JLabel)((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(0)).getComponent(0)).setHorizontalAlignment(JLabel.CENTER);
      ((JPanel)((JPanel) this.getContentPane().getComponent(0)).getComponent(2)).setVisible(false);

		}

		//Vérifications du bouton jouer :
		int n = 0;
			while(n < game.getBoard().getNbRows() && game.getBoard().getSquare(n, square).getCoin() != null)n++;
			if(n == game.getBoard().getNbRows())
				ajout.setEnabled(false);
			else
				ajout.setEnabled(true);
  }
  
  private ActionListener onChangeCoin = new ActionListener() {
    public void actionPerformed(ActionEvent arg0) {
      ((PowerUpController)controleur).nextCoinPowerUp();
    }
  };

  
}
