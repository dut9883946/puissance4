package connectfour.connectfourpowerup;

import connectfour.connectfourmodel.Board;
import connectfour.connectfourmodel.Coin;
import connectfour.connectfourmodel.Game;
import connectfour.connectfourmodel.PowerUp;
import connectfour.connectfourmodel.Square;
import connectfour.connectfourpopout.PopOutController;

public class PowerUpController extends PopOutController {

  private Coin coin;

  /**
   * @see PopOutController
   */
  public PowerUpController(Game game, int pointsToWin, int coinsToAlign) {
    super(game, pointsToWin, coinsToAlign);
    coin = player.getCoin(0);
  }

  /**
   * Passe au type de jeton suivant.
   */
  public void nextCoinPowerUp() {
    int i = 0;
    if(player.stillHaveSpecialCoins()){
      while(player.getCoin(i) != coin) i++;
      while(player.getCoin(i).getPowerUp() == coin.getPowerUp()) {
        if(i== player.getCoinNumber() - 1)
          i = 0;
        else
          i++;
      }
    }
    coin = player.getCoin(i);
    notifyObservers();
  }

  @Override
  public void forward() {
    verifyPoints();
    verifyWinner();
    if(game.getBoard().isFull())
      game.setIsOver();
    if(!game.isOver()){
      player = player == game.getPlayer(0) ? game.getPlayer(1) : game.getPlayer(0);
      coin = player.getCoin(0);
    }
    notifyObservers();
  }

  @Override
  public void playCoin(int coinIndex, int column) {
    int i = 0;
    while(player.getCoin(i) != coin) i++;
    if(player.getCoin(i).getPowerUp() == PowerUp.ANVIL){
      for(Square[] row : game.getBoard().getAllSquares())
        row[column].setCoin(null);
      super.playCoin(i, column);
    }
    else if(player.getCoin(i).getPowerUp() == PowerUp.PLAY2 || player.getCoin(i).getPowerUp() == PowerUp.WALL){
      Board board = game.getBoard();
      //On cherche la première case vide sur la colonne
      coinIndex = 0;
      i=0;
      while(i < board.getNbRows() && board.getSquare(i, column).getCoin() != null) i++;
      if(i != board.getNbRows()){  //Si il existe une case vide dans la colonne
        board.getSquare(i, column).setCoin(coin); //on place le jeton
        player.playCoin(coinIndex); // on le supprime des jetons restants du joueur
        verifyPoints();
        verifyWinner();
        if(game.getBoard().isFull())
          game.setIsOver();
        coin = player.getCoin(0);
      }
      notifyObservers();
    }
    else
      super.playCoin(i, column);
  }

  /**
   * Renseigne le jeton courant.
   */
  public Coin getCoin() { return coin; }
  
}
