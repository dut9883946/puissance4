package connectfour.connectfourpopout;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import connectfour.connectfourcontroller.TemplateController;
import connectfour.connectfourmodel.Game;
import connectfour.connectfourview.TemplateGameView;

public class PopOutView extends TemplateGameView {

  protected JButton ejecter;

  /**
   * @see TemplateGameView
   */
  public PopOutView(PopOutController controleur, Game game) {
    super(controleur, game);
    ejecter = new JButton("Éjecter");
    ejecter.setFont(new Font("Arial", Font.BOLD, 20));
    ejecter.addActionListener(onPopOut);
    ejecter.setEnabled(false);
    bouton.add(ejecter, 2);
    this.setTitle("Pop Out, "+game.getBoard().getNbColumns()+" x "+game.getBoard().getNbRows());
    this.setVisible(true);
  }

  @Override public void update() {
    super.update();
    //Vérifications du bouton éjecter :
    if(game.getBoard().getSquare(0, square).getCoin() != null && game.getBoard().getSquare(0, square).getCoin().getColor() == controleur.getCurrentPlayer().getColor())
      ejecter.setEnabled(true);
    else
      ejecter.setEnabled(false);
  }

  private ActionListener onPopOut = new ActionListener() {
    public void actionPerformed(ActionEvent arg0) {
      ((PopOutController)controleur).popCoin(square);
    }
  };
  
}
