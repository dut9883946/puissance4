package connectfour.connectfourpopout;

import connectfour.connectfourcontroller.TemplateController;
import connectfour.connectfourmodel.Board;
import connectfour.connectfourmodel.Coin;
import connectfour.connectfourmodel.Game;

public class PopOutController extends TemplateController {

  /**
   * @see TemplateController
   */
  public PopOutController(Game game, int pointsToWin, int coinsToAlign) {
    super(game, pointsToWin, coinsToAlign);
  }

  /**
   * Éjecte du jeu le jeton sur la ligne 0 et la colonne demandé, si ce dernier appartient au joueur courant.
   * 
   * @param column colonne sur laquelle jouer
   */
  public void popCoin(int column) {
    Board board = game.getBoard();
    // S'il y a un jeton sur la case et qu'il appartient au joueur courant, on l'éjecte
    if(board.getSquare(0, column).getCoin() != null && board.getSquare(0, column).getCoin().getColor() == player.getColor()) {
      player.addCoin(new Coin(player.getColor()));
      for(int i=0; i< board.getNbRows() - 1 ; i++)
        board.getSquare(i, column).setCoin(board.getSquare(i+1, column).getCoin());
      board.getSquare(board.getNbRows() - 1, column).setCoin(null);
      forward();
    }
  }
  
}
