package connectfour.connectfourpop10;

import connectfour.connectfourcontroller.TemplateController;
import connectfour.connectfourmodel.Game;
import connectfour.connectfourpopout.PopOutController;

public class Pop10Controller extends PopOutController {

  private int gamePhase;

  /**
   * @see TemplateController
   */
  public Pop10Controller(Game game, int pointsToWin, int coinsToAlign) {
    super(game, pointsToWin, coinsToAlign);
    this.gamePhase = 0;
  }

  /**
   * Fait avancer la partie d'un pas élémentaire.
   * 
   * @see TemplateController
   */
  @Override
  public void forward() {
    if(gamePhase == 0){
      if(game.getBoard().isFull())
        gamePhase=1;
      player = player == game.getPlayer(0) ? game.getPlayer(1) : game.getPlayer(0);
    }
    else{
      verifyWinner();
      if(game.getPlayer(0).getPoints()+game.getPlayer(1).getPoints() == game.getBoard().getEmptySquares())
        player = player == game.getPlayer(0) ? game.getPlayer(1) : game.getPlayer(0);
    }
    notifyObservers();
  }

  /**
   * Vérifie si un joueur peut jouer un jeton et le joue le cas échéant.
   * 
   * @see TemplateController
   */
  @Override
  public void playCoin(int coinIndex, int column) {
    if(gamePhase == 0) {
      int row = game.getBoard().getFirstPlayableRow();
      //Si il y a une ligne de libre et que le joueur essai de jouer sur cette dernière, on accepte
      if(row != -1 && game.getBoard().getSquare(row, column).getCoin() == null){
        game.getBoard().getSquare(row, column).setCoin(player.getCoin(coinIndex)); //on place le jeton
        player.playCoin(coinIndex); // on le supprime des jetons restants du joueur
        forward();
      }
    }
    else
      if(game.getPlayer(0).getPoints()+game.getPlayer(1).getPoints() != game.getBoard().getEmptySquares())
        super.playCoin(coinIndex, column);
  }

  @Override
  public void popCoin(int column) {
    if(gamePhase == 1 && game.getPlayer(0).getPoints()+game.getPlayer(1).getPoints() == game.getBoard().getEmptySquares()){
      if(isInConnect4(column))
        player.score();
      super.popCoin(column);
    }
  }

  private boolean isInConnect4(int column) {
    boolean res = false;
    int counter, counterBuffer = 0;
    int testRow, testCol;
    int rowDir, colDir;
    for(int dir=0; dir< 5;dir++){
      counter = 0;
      testRow = 0;
      testCol = column;
      switch(dir){
        case 0:
          rowDir = 0;
          colDir = -1;
          break;
        case 1:
          rowDir = 1;
          colDir = -1;
          break;
        case 2:
          rowDir = 1;
          colDir = 0;
          break;
        case 3:
          rowDir = 1;
          colDir = 1;
          break;
        default:
          rowDir = 0;
          colDir = 1;
          break;
      }
      while(counter < coinsToAlign && 
        game.getBoard().getSquare(0, column).getCoin() != null &&
        testRow >= 0 && testCol >= 0 && testRow < game.getBoard().getNbRows() && testCol < game.getBoard().getNbColumns() &&
        game.getBoard().getSquare(testRow, testCol).getCoin() != null &&
        game.getBoard().getSquare(0, column).getCoin().getColor() == game.getBoard().getSquare(testRow, testCol).getCoin().getColor()){
        counter++;
        testRow += rowDir;
        testCol += colDir;
        if(dir == 0)
          counterBuffer++;
      }
      if(counter >= coinsToAlign || ( dir == 4 && counter + counterBuffer > coinsToAlign))
        res = true;
    }
    return res;
  }

  /**
   * Retourne la phase de jeu actuelle.
   *  
   * @return phase de jeu
   */
  public int getGamePhase(){ return gamePhase; }
  
}
