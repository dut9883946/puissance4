package connectfour.connectfourpop10;

import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import connectfour.connectfourmodel.Game;
import connectfour.connectfourpopout.PopOutView;

public class Pop10View extends PopOutView {

  public Pop10View(Pop10Controller controleur, Game game) {
    super(controleur, game);
    
    this.setTitle("Pop 10, "+game.getBoard().getNbColumns()+" x "+game.getBoard().getNbRows());

    JLabel pointJ1=new JLabel("Point J1 : "+game.getPlayer(0).getPoints());
    ImageIcon jetonJaune = new ImageIcon(getClass().getClassLoader().getResource("image/jetonJaune.png"));
    jetonJaune= new ImageIcon(jetonJaune.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
    ImageIcon jetonRouge = new ImageIcon(getClass().getClassLoader().getResource("image/jetonRouge.png"));
    jetonRouge= new ImageIcon(jetonRouge.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
    pointJ1.setIcon(jetonJaune);
    pointJ1.setFont(new Font("Arial", Font.BOLD, 20));
    JLabel pointJ2=new JLabel("Point J2 : "+game.getPlayer(1).getPoints());
    pointJ2.setIcon(jetonRouge);
    pointJ2.setFont(new Font("Arial", Font.BOLD, 20));
    JPanel JPpoint=new JPanel();
    JPpoint.setLocation(this.getContentPane().getComponent(0).getSize().width+5, 200);
    JPpoint.setSize(200,200);
    JPpoint.add(pointJ1, BorderLayout.NORTH);
    JPpoint.add(pointJ2, BorderLayout.SOUTH);
    this.add(JPpoint);
    this.setSize(this.getSize().width+70,this.getSize().height-100);
  }

  @Override 
  public void update() {
    super.update();

    ((JLabel)((JPanel)this.getContentPane().getComponent(1)).getComponent(0)).setText("Point J1 : "+game.getPlayer(0).getPoints());
    ((JLabel)((JPanel)this.getContentPane().getComponent(1)).getComponent(1)).setText("Point J2 : "+game.getPlayer(1).getPoints());

    //Vérifications du bouton jouer :
    int row = game.getBoard().getFirstPlayableRow();
    int gamePhase = ((Pop10Controller)controleur).getGamePhase();
    if((gamePhase == 0 && row != -1 && game.getBoard().getSquare(row, square).getCoin() == null) || (gamePhase == 1 && game.getPlayer(0).getPoints()+game.getPlayer(1).getPoints() != game.getBoard().getEmptySquares()))
      ajout.setEnabled(true);
    else
      ajout.setEnabled(false);

    //Vérifications du bouton éjecter :
    if(gamePhase == 1 && game.getPlayer(0).getPoints()+game.getPlayer(1).getPoints() == game.getBoard().getEmptySquares() && game.getBoard().getSquare(0, square).getCoin() != null && game.getBoard().getSquare(0, square).getCoin().getColor() == controleur.getCurrentPlayer().getColor())
      ejecter.setEnabled(true);
    else
      ejecter.setEnabled(false);
  }

}
