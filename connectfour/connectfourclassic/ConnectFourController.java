package connectfour.connectfourclassic;

import connectfour.connectfourcontroller.TemplateController;
import connectfour.connectfourmodel.Game;

public class ConnectFourController extends TemplateController{

  /**
   * @see TemplateController
   */
  public ConnectFourController(Game game, int pointsToWin, int coinsToAlign) {
    super(game, pointsToWin, coinsToAlign);
  }
  
}
