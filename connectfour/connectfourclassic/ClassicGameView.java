package connectfour.connectfourclassic;

import connectfour.connectfourcontroller.TemplateController;
import connectfour.connectfourmodel.Game;
import connectfour.connectfourview.TemplateGameView;

public class ClassicGameView extends TemplateGameView {

  public ClassicGameView(TemplateController controleur, Game game) {
    super(controleur, game);
    if(controleur.getCoinsToAlign()==4)
      this.setTitle("Classic, "+game.getBoard().getNbColumns()+" x "+game.getBoard().getNbRows());
    else
      this.setTitle("5 in a row, "+game.getBoard().getNbColumns()+" x "+game.getBoard().getNbRows());
  }
  
}
