package connectfour.connectfourcontroller;

import connectfour.connectfourmodel.Board;
import connectfour.connectfourmodel.Game;
import connectfour.connectfourmodel.Player;
import connectfour.connectfourmodel.PowerUp;
import observersubject.Subject;

public abstract class TemplateController extends Subject {
  
  protected Game game;

  protected Player player;

  protected int pointsToWin;  //Nombre de points pour gagner la partie

  protected int coinsToAlign; //Nombre de jetons à aligner pour marquer un point


  // Constructeur

  /**
   * Construit un patron ("template") de contrôleur et désigne le premier joueur.
   * 
   * @param game modèle de partie de jeu
   * @param pointsToWin nombre de points pour gagner
   * @param coinsToAlign nombre de jetons à aligner pour marquer un point
   */
	public TemplateController(Game game, int pointsToWin, int coinsToAlign) {
		this.game = game;
    this.pointsToWin = pointsToWin;
    this.coinsToAlign = coinsToAlign;
		this.player = game.getPlayer(0);  //On désigne le premier joueur
  }
  
  /**
   * Fait avancer la partie d'un pas élémentaire.
   * 
	 * Chaque pas fait intervenir un joueur pour poser une carte ou 
	 * distribuer les cartes pour une nouvelle donne
   */
  public void forward() {
    verifyPoints();
    verifyWinner();
    if(game.getBoard().isFull())
      game.setIsOver();
    if(!game.isOver())
      player = player == game.getPlayer(0) ? game.getPlayer(1) : game.getPlayer(0);
    notifyObservers();
  }

  /**
   * Joue un jeton sur la colonne renseignee
   * 
   * @param coinIndex index du jeton à jouer
   * @param column colonne sur laquelle jouer
   */
  public void playCoin(int coinIndex, int column) {
    Board board = game.getBoard();
    //On cherche la première case vide sur la colonne
    int i = 0;
    while(i < board.getNbRows() && board.getSquare(i, column).getCoin() != null) i++;
    if(i != board.getNbRows()){  //Si il existe une case vide dans la colonne
      board.getSquare(i, column).setCoin(player.getCoin(coinIndex)); //on place le jeton
      player.playCoin(coinIndex); // on le supprime des jetons restants du joueur
      forward();
    }
  }

  /**
   * Vérifie si un des joueurs a marqué et lui ajoute ses points en appelant la méthode récursive
   */
  protected void verifyPoints() {
    verifyPoints(0, 0, 0, 0, 1);
  }

  /** 
   * Vérifie récursivement si un joueur marque un point, et lui attribue
   * si tel est le cas.
   * 
   * @param rowSquare ligne de la case de départ
   * @param colSquare colonne de la case de départ
   * @param rowNextSquare ligne de la case suivante dans la vérification courante
   * @param colNextSquare colonne de la case suivante dans la vérification courante
   * @param counter compteur de connexions de la vérification courante
   */
  protected void verifyPoints(int rowSquare, int colSquare, int rowNextSquare, int colNextSquare, int counter) {
    if(rowSquare == rowNextSquare && colSquare == colNextSquare){
      //Si on est au dessus du tableau ou que la case est vide
      if(rowSquare >= game.getBoard().getNbRows() || game.getBoard().getSquare(rowSquare, colSquare).getCoin() == null){
        if(colSquare != game.getBoard().getNbColumns() - 1) //Si on est a la dernière colonne, on s'arrete
          verifyPoints(0, colSquare + 1, 0, colSquare + 1, counter);
      }
      else
        verifyPoints(rowSquare, colSquare, rowNextSquare + 1, colNextSquare, 1);
    }
    else{
      int rowDir = rowNextSquare == rowSquare ? 0 : rowNextSquare > rowSquare ? 1 : -1;
      int colDir = colNextSquare == colSquare ? 0 : colNextSquare > colSquare ? 1 : -1;
      //Si on est dans le tableau et que la couleur est la même :
      if(rowNextSquare >= 0 && colNextSquare >= 0 && rowNextSquare < game.getBoard().getNbRows() && colNextSquare < game.getBoard().getNbColumns() && 
        game.getBoard().getSquare(rowSquare, colSquare).getCoin() != null && game.getBoard().getSquare(rowNextSquare, colNextSquare).getCoin() != null &&
        game.getBoard().getSquare(rowSquare, colSquare).getCoin().getColor() == game.getBoard().getSquare(rowNextSquare, colNextSquare).getCoin().getColor() &&
        game.getBoard().getSquare(rowNextSquare, colNextSquare).getCoin().getPowerUp() != PowerUp.WALL && 
        game.getBoard().getSquare(rowSquare, colSquare).getCoin().getPowerUp() != PowerUp.WALL){
        if(++counter == coinsToAlign){  //Un joueur marque un point
          if(game.getBoard().getSquare(rowSquare, colSquare).getCoin().getColor() == game.getPlayer(0).getColor())
            game.getPlayer(0).score();
          else
            game.getPlayer(1).score();
        }
        else
          verifyPoints(rowSquare, colSquare, rowNextSquare + rowDir, colNextSquare + colDir, counter);  //On continue dans cette direction
      }
      else{
        //On change de direction :
        if(rowDir == 1 && colDir == 0){  //Si N, NE 
          verifyPoints(rowSquare, colSquare, rowSquare+1, colSquare +1, 1);
        }
        else if(rowDir == 1 && colDir == 1){ //Si NE, E
          verifyPoints(rowSquare, colSquare, rowSquare, colSquare +1, 1);
        }
        else if(rowDir == 0 && colDir == 1){ //Si E, SE
          verifyPoints(rowSquare, colSquare, rowSquare -1, colSquare +1, 1);
        }
        else//Si on a fait toutes les directions, on change de case
          verifyPoints(rowSquare+1, colSquare, rowSquare+1, colSquare, 1);
      }
    }
  }

  /**
   * Vérifie si un joueur à gagné la partie
   */
  protected void verifyWinner() {
    for(int i=0; i< 2; i++)
      if(game.getPlayer(i).getPoints() >= pointsToWin){
        game.setIsOver();
        if(game.getPlayer(i).getPoints() > game.getPlayer(1-i).getPoints())
          game.setWinner(game.getPlayer(i));
      }
  }


  //Accesseurs :

  /**
   * Renseigne le joueur courant
   * 
   * @return joueur courant
   */
  public Player getCurrentPlayer() { return player; }

  /**
   * Renseigne le nombre de jeton a aligner
   * 
   * @return nombre de jeton à aligner
   */
  public int getCoinsToAlign() { return coinsToAlign; }

  
}
